<?php
/**
 * 全局配置文件
 */

defined('PATH_ROOT') || define('PATH_ROOT', realpath(dirname(__FILE__) . '../../'));

define('PATH_APP',  PATH_ROOT .  '/app' );

define('PATH_LIB', PATH_ROOT . '/lib');