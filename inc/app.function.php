<?php
function exception_handler($exception) {

    if (!isset($exception->request_params)) {
        $exception->request_params = null;
    }

   $error_html = sprintf('
     <!doctype html>
        <html lang="en">
         <head>
          <meta charset="UTF-8">
          <title>Eorror Message</title>
          </head>
         <body>
         <h1>应用程序错</h1>
          <h3>异常信息：</h3>
          <p  style="border: 1px solid red;padding: 10px;background-color: #FFEFBF;">
              <b>信息：</b>%s
        </p>
        <h3>错误线索：</h3>
        <pre  style="border: 1px solid red;padding: 10px;background-color: #FFEFBF;">%s</pre>

        <h3>请求参数：</h3>
        <pre  style="border: 1px solid red;padding: 10px;background-color: #FFEFBF;">%s</pre>
        </body>
    </html>', $exception->getMessage(), $exception->getTraceAsString(), var_export($exception->request_params, true));

    echo $error_html;

}

set_exception_handler('exception_handler');