<?php

/**
 *  Model - 数据层抽象类
 *  提供对数据表进行CRUD功能
 */
abstract class Model
{

    /**
     * 前端控制器实例
     *
     * @var ControllerFront
     */
    protected $_front_controller = null;

    /**
     * 当前操作数据库实例
     * @var null
     */
    protected $_db = null;

    /**
     * 当前操作的数据库表名
     * @var string
     */
    protected $_handle_table = '';

    /**
     * 当前数据库表列表
     * @var array
     */
    protected $_list_tables = array();

    /**
     * 结果集的获取模式
     * @var int
     */
    protected $_fetch_mode = PDO::FETCH_ASSOC;


    /**
     * 构造方法
     *
     * @param $option array 数据库配置参数
     */
    public function __construct($option)
    {
        class_exists('Db', false) || require 'rthink/db/Db.php';
        $this->_db = Db::singleton($option);
        $this->init();
    }


    protected function init()
    {
        if ($this->getFrontController()->getRequest()->getParam('rdebug') !== null) {
            $db_profilers = $this->getFrontController()->getRequest()->getParam('db_profilers');
            $db_profilers || $db_profilers = array();
            $this->_db->getProfiler()->setEnabled(true);
            $db_profilers[] = $this->_db->getProfiler();
            $this->getFrontController()->getRequest()->setParam('db_profilers', $db_profilers);
        }

        $this->_list_tables = $this->_db->listTables();
    }

    /**
     * 设置当前的操作表
     *
     * @return $this
     */
    public function setHandleTable($table)
    {
        if (!in_array($table, $this->_list_tables)) {
            class_exists('DbException') || require 'rthink/db/DbException.php';
            throw new DbException ("Invalid table '$table' specified");
        }

        $this->_handle_table = $table;

        return $this;
    }

    /**
     * 获取数据库实例
     *
     * @return null
     */
    public function getDbInstance()
    {
        return $this->_db;
    }


    /**
     *  添加记录
     *
     * @param array $bind
     * @return bool
     */
    public function add(array $bind)
    {
        $ins_res = $this->_db->insert($this->_handle_table, $bind);

        if ($ins_res > 0) {
            return $this->_db->lastInsertId();
        }

        return false;
    }

    /**
     *  修改记录
     *
     * @param array $fields
     * @param $where
     *
     * @return int 影响的行数
     */
    public function update(array $fields, $where)
    {
        $where = $this->handleWhereExpr($where);
        return $this->_db->update($this->_handle_table, $fields, $where);
    }

    /**
     * 删除记录
     *
     * @param string $table 操作表
     * @param mixed $where where条件
     * @return int 影响的行数
     */
    public function delete($where = array())
    {
        $where = $this->handleWhereExpr($where);
        return $this->_db->delete($this->_handle_table, $where);
    }

    /**
     * 获取结果集中所有记录
     *
     * @param array $option
     * @return array
     */
    public function fetchAll(array $option = array())
    {
        $opt_res = $this->handleOpt($option);
        $opt_res += array('fields' => '*', 'condition' => '', 'bind' => array());
        $sql = 'SELECT ' . $opt_res['fields'] . ' FROM ' . $this->_db->quoteIdentifier($this->_handle_table) . $opt_res['condition'];

        return $this->_db->fetchAll($sql, $opt_res['bind'], $this->_fetch_mode);
    }


    /**
     * 获取结果集中第一条记录
     *
     * @param array $option
     * @return array
     */
    public function fetchRow($option = array())
    {
        $opt_res = $this->handleOpt($option);
        $opt_res += array('fields' => '*', 'condition' => '', 'bind' => array());
        $sql = 'SELECT ' . $opt_res['fields'] . ' FROM ' . $this->_db->quoteIdentifier($this->_handle_table) . $opt_res['condition'];

        return $this->_db->fetchRow($sql, $opt_res['bind'], $this->_fetch_mode);
    }

    /**
     * 获取结果集中所有记录并以关联数组的形式返回
     * 此结果集数组以每条记录的第一列的值为 key, 以每一条记录为 value 的索引数组. 如果两条记录的第一列的值相同,
     * 那么后面那条记录会覆盖前面的记录.
     *
     * @param array $option
     * @return array
     */
    public function fetchAssoc($option = array())
    {
        $opt_res = $this->handleOpt($option);
        $opt_res += array('fields' => '*', 'condition' => '', 'bind' => array());
        $sql = 'SELECT ' . $opt_res['fields'] . ' FROM ' . $this->_db->quoteIdentifier($this->_handle_table) . $opt_res['condition'];

        return $this->_db->fetchAssoc($sql, $opt_res['bind']);
    }


    /**
     * 获取结果集中所有记录的第一列以索引数组的形式返回
     *
     * @param array $option
     * @return array
     */
    public function fetchCol($option = array())
    {
        $opt_res = $this->handleOpt($option);
        $opt_res += array('fields' => '*', 'condition' => '', 'bind' => array());
        $sql = 'SELECT ' . $opt_res['fields'] . ' FROM ' . $this->_db->quoteIdentifier($this->_handle_table) . $opt_res['condition'];

        return $this->_db->fetchCol($sql, $opt_res['bind']);
    }

    /**
     *  设置结果集获取模式
     * @param $fetch_mode
     * @return $this
     */
    public function setFetchMode($fetch_mode)
    {
        $this->_fetch_mode = $fetch_mode;
        return $this;
    }

    /**
     *  处理sql查询条件
     *
     * @param array $option
     * @return array
     */
    protected function handleOpt($option)
    {
        //处理结果
        $handle_res = array();

        $placeholder = array();
        $bind = array();
        $condition = '';

        if (isset($option['fields'])) {
            $handle_res['fields'] = '';
            foreach ((array)$option['fields'] as $field) {
                $handle_res['fields'] .= $this->_db->quoteIdentifier($field) . ',';
            }
            $handle_res['fields'] = rtrim($handle_res['fields'], ',');
        }

        if (isset($option['where'])) {
            $where_con = array();

            foreach ($option['where'] as $c_key => $c_val) {
                if ($count = substr_count($c_key, '?')) {
                    if ($count == 1) {
                        $where_con[] = '(' . $this->_db->quoteInto($c_key, $c_val) . ')';
                    } else {
                        $or_con = explode('?', $c_key);
                        $or_parts = array();

                        foreach ($or_con as $or_key => $or_val) {
                            if (!isset($c_val[$or_key])) {
                                continue;
                            }

                            $or_val .= '?';
                            $or_parts[] = $this->_db->quoteInto($or_val, $c_val[$or_key]);
                        }

                        $where_con[] = '(' . join(' ', $or_parts) . ')';
                    }
                } else {
                    $placeholder[] = "$c_key=:$c_key";
                    $bind[":$c_key"] = $c_val;
                }
            }

            if (!empty($where_con)) {
                $condition .= ' WHERE ' . join(' AND ', $where_con);
            } else if (!empty($placeholder)) {
                $condition .= ' WHERE ' . join(' AND ', $placeholder);
            }
        }

        if (isset($option['order'])) {
            $condition .= ' ORDER BY ' . $option['order'];
        }

        if (isset($option['limit'])) {
            $option['limit']['offset'] = isset($option['limit']['offset']) ? intval($option['limit']['offset']) : 0;
            $condition = $this->_db->limit($condition, intval($option['limit']['count']), $option['limit']['offset']);
        }

        empty($condition) ||
        $handle_res['condition'] = $condition;
        empty($bind) ||
        $handle_res['bind'] = $bind;

        return $handle_res;
    }


    /**
     *  处理update delete 操作的where条件
     *
     * @param array $option
     * @return array
     */
    protected function handleWhereExpr(array $where)
    {
        $handle_res = array();

        foreach ($where as $key => $val) {
            $key = trim($key, ' =<>');
            $handle_res["$key=?"] = $val;
        }

        return $handle_res;
    }


    /**
     * 获取前端控制器
     *
     * @return ControllerFront
     */
    protected function getFrontController()
    {
        // Used cache version if found
        if (null === $this->_front_controller) {
            class_exists('ControllerFront') || require 'rthink/ControllerFront.php';
            $this->_front_controller = ControllerFront::getInstance();
        }

        return $this->_front_controller;
    }

}