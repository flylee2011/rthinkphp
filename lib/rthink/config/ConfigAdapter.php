<?php

/**
 * ConfigAdapter
 *
 * 配置文件解析基类
 * 为了方便访问配置文件数据实现了 Countable, Iterator接口
 * 且当$_allow_modification为true时，才可以修改配置项容器的数据
 *
 * @uses Countable
 * @uses Iterator
 * @package
 * @version $id$ 2013/01/23
 * @author xiaohuzi <thehopefield@gmail.com>
 */
class ConfigAdapter implements Countable, Iterator
{
    /**
     * _allow_modification
     * 记忆配置选项是否可修改
     *
     * @var boolean
     * @access protected
     */
    protected $_allow_modification;

    /**
     * _index
     * 迭代下标
     *
     * @var integer
     * @access protected
     */
    protected $_index;

    /**
     * _count
     * 配置选项数量
     *
     * @var integer
     * @access protected
     */
    protected $_count;

    /**
     * _data
     * 配置项容器
     *
     * @var array
     * @access protected
     */
    protected $_data;

    /**
     * Used when unsetting values during iteration to ensure we do not skip the
     * next element
     *
     * @access protected
     * @var boolean
     */
    protected $_skip_next_iteration;

    /**
     * _loaded_section
     * 配置文件加载的片段
     *
     * @var string
     * @access protected
     */
    protected $_loaded_section;

    /**
     * _error_str
     * 加载文件的错误信息 如果为空则没有错误产生
     *
     * @var string
     * @access protected
     */
    protected $_error_str = null;

    /**
     * __construct
     * 构造方法
     *
     * @access public
     * @param array $array
     * @param boolean $allow_modification
     *
     * @return void
     */
    public function __construct(array $array, $allow_modification = false)
    {
        $this->_allow_modification = ( boolean )$allow_modification;
        $this->_loaded_section = null;
        $this->_data = $array;
        $this->_count = count($this->_data);
    }

    /**
     * get
     * 获取指定key的配置项 不存在则返回空
     *
     * @access public
     * @param string $name
     * @param mixed $default
     *
     * @return mixed
     */
    public function get($key, $default = null)
    {
        $result = $default;
        $key = str_replace('_', '.', $key);
        if (array_key_exists($key, $this->_data)) {
            $result = $this->_data [$key];
        }
        return $result;
    }

    /**
     * __get
     * Magic function so that $obj->value will work.
     *
     * @access public
     * @param string $key
     *
     * @return mixed
     */
    public function __get($key)
    {
        return $this->get($key);
    }

    /**
     * __set
     * Only allow setting of a property if $allow_modification was set to TRUE
     * on construction. Otherwise, throw an exception.
     *
     * @access public
     * @param mixed $name
     * @param mixed $value
     *
     * @throws ConfigException
     * @return void
     */
    public function __set($key, $value)
    {
        if ($this->_allow_modification) {
            $key = str_replace('_', '.', $key);
            $this->_data [$key] = $value;
            $this->_count = count($this->_data);
        } else {
            /**
             * @see ConfigException
             */
            class_exists('ConfigException', false) || require 'rthink/config/ConfigException.php';
            throw new ConfigException ('Config is read only');
        }
    }


    /**
     * __clone
     * Deep clone of this instance to ensure that nested Configs are also
     * cloned.
     *
     * @access public
     * @return void
     */
    public function __clone()
    {
        $array = array();
        foreach ($this->_data as $key => $value) {
            if ($value instanceof ConfigAdapter) {
                $array [$key] = clone $value;
            } else {
                $array [$key] = $value;
            }
        }
        $this->_data = $array;
    }

    /**
     * __isset
     * PHP 5.1 起支持isset()重载
     *
     * @access public
     * @param string $key
     *
     * @return boolean
     */
    public function __isset($key)
    {
        return isset ($this->_data [$key]);
    }

    /**
     * __unset
     * PHP 5.1上支持unset()重载
     *
     * @access public
     * @param string $key
     * @throws ConfigException
     *
     * @return void
     */
    public function __unset($key)
    {
        if ($this->_allow_modification) {
            unset ($this->_data [$key]);
            $this->_count = count($this->_data);
            $this->_skip_next_iteration = TRUE;
        } else {
            /**
             * @see ConfigException
             */
            class_exists('ConfigException', false) || require 'rthink/config/ConfigException.php';
            throw new ConfigException ('Config is read only');
        }
    }


    /**
     * count
     * Countable interface 定义的抽象方法
     *
     * @access public
     * @return integer
     */
    public function count()
    {
        return $this->_count;
    }


    /**
     * current
     * Iterator interface 定义的抽象方法
     *
     * @access public
     * @return mixed
     */
    public function current()
    {
        $this->_skip_next_iteration = false;
        return current($this->_data);
    }


    /**
     * key
     * Iterator interface 定义的抽象方法
     *
     * @access public
     * @return mixed
     */
    public function key()
    {
        return key($this->_data);
    }


    /**
     * next
     * Iterator interface 定义的抽象方法
     *
     * @access public
     * @return void
     */
    public function next()
    {
        if ($this->_skip_next_iteration) {
            $this->_skip_next_iteration = false;
            return;
        }
        next($this->_data);
        $this->_index++;
    }


    /**
     * rewind
     * Iterator interface 定义的抽象方法
     *
     * @access public
     * @return void
     */
    public function rewind()
    {
        $this->_skip_next_iteration = false;
        reset($this->_data);
        $this->_index = 0;
    }


    /**
     * valid
     * Iterator interface 定义的抽象方法
     *
     * @access public
     * @return boolean
     */
    public function valid()
    {
        return $this->_index < $this->_count;
    }

    /**
     * getSectionName
     * Returns the section name loaded.
     *
     * @access public
     * @return mixed
     */
    public function getSectionName()
    {
        return $this->_loaded_section;
    }

    /**
     * setReadOnly
     * Prevent any more modifications being made to this instance. Useful after
     * merge() has been used to merge multiple Config objects into one object
     * which should then not be modified again.
     *
     * @access public
     * @return void
     */
    public function setReadOnly()
    {
        $this->_allow_modification = false;
    }

    /**
     * readOnly
     * Returns if this Config object is read only or not.
     *
     * @access public
     * @return boolean
     */
    public function readOnly()
    {
        return !$this->_allow_modification;
    }

    /**
     * _loadFileErrorHandler
     * Handle any errors from parse_ini_file
     *
     * @access protected
     * @param integer $errno
     * @param string $errstr
     * @param string $errfile
     * @param integer $errline
     *
     * @return void
     */
    protected function _loadFileErrorHandler($errno, $errstr, $errfile, $errline)
    {
        if ($this->_error_str === null) {
            $this->_error_str = $errstr;
        } else {
            $this->_error_str .= (PHP_EOL . $errstr);
        }
    }

    public function getData()
    {
        return $this->_data;
    }
}
