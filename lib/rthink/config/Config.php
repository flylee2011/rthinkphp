<?php

/**
 * 配置文件处理类
 */
class Config
{
    /**
     * 解析器单例集合
     *
     * @var array
     */
    protected static $_parser = array();

    /**
     * 配置信息缓存
     *
     * @var array
     */
    protected static $_config_cache = array();

    /**
     * 配置文件解析对象创建工厂
     *
     * @param string $parser_type 解析器的类型
     * @param string $filename 配置文件路径
     * @param array $option
     * @return ConfigAdapter
     */
    public static function factory($parser_type, $file, $option = array())
    {
        $class_name = ucfirst($parser_type) . 'Adapter';
        class_exists($class_name, false) || require 'rthink/config/' . $class_name . '.php';

        $option += array(
            'section' => '',
            'allow_modify' => false
        );
        return new $class_name ($file, $option ['section'], $option ['allow_modify']);
    }

    /**
     * 以单例的形式创建配置文件解析对象 由于是单例模式, 所以当引用单例的时候, $options 参数将会失去意义, 也就是说
     * 只有在创建单例的第一次调用的时候, $options 才有意义.
     *
     * @param string $parser_type 解析器的类型
     * @param array $options
     * @return ConfigAdapter
     */
    public static function singleton($parser_type, $file, $options = array())
    {
        // 实现每种数据库的单例
        if (isset (self::$_parser [$parser_type]) && self::$_parser [$parser_type] instanceof ConfigAdapter) {
            return self::$_parser [$parser_type];
        }

        self::$_parser [$parser_type] = self::factory($parser_type, $file, $options);

        return self::$_parser [$parser_type];
    }

    /**
     * 获取指定键值的配置参数
     *
     * @param string $key
     * @return null string array
     */
    public static function get($key)
    {
        if (isset (self::$_config_cache [$key])) {
            return self::$_config_cache [$key];
        }

        class_exists('ControllerFront', false) || require 'rthink/controller/ControllerFront.php';

        $config_parser = ControllerFront::getInstance()->getParam('config_parser');
        if (null == $config_parser) {
            $config_parser = 'ini';
        }
        $config_file = ControllerFront::getInstance()->getParam('config_file');
        $config_section = ControllerFront::getInstance()->getParam('config_section');

        $parser = self::singleton($config_parser, $config_file, array(
            'section' => $config_section
        ));
        return self::$_config_cache [$key] = $parser->get($key);
    }

    public static function set()
    {
        // @todo
    }
}