<?php
/** @see rthinkException * */
require_once 'rthink/exception/rthinkException.php';

/**
 * 配置文件解析异常类
 */
class ConfigException extends RThinkException
{
}