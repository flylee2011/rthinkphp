<?php
require 'ConfigAdapter.php';

/**
 * ConfigIniAdapter  从ini配置文件中加载指定配置片段 如果键名里包含一个或多个 ".", 左起至第一个"."子字符串为对同一个项目的进行设置的一组属性
 * example ini file:
 * <code>
 * [development]
 * db.host = localhost
 * db.database = test
 * db.connection = database
 * php.display_errors = On
 * hostname = live
 * [production]
 * hostname = staging
 * </code>
 * 使用方法
 * <code>
 * $data = new ConfigIni($file, 'development');
 *  $data->get('db.host');
 *  $data->get('db');
 *  $data->hostname = "staging"
 * </code>
 *
 * @version $id$ 2013/01/23
 * @author xiaohuzi <thehopefield@gmail.com>
 */
class IniAdapter extends ConfigAdapter
{
    /**
     * __construct
     *
     * @access public
     * @param string $filename 配置文件名
     * @param string $section 加载的配置文件片段
     * @param boolean $allow_modify 配置修改标识
     * @throws ConfigException
     * @return void
     */
    public function __construct($filename, $section, $allow_modify = false)
    {
        if (empty ($filename)) {
            /**
             * @see ConfigException
             */
            class_exists('ConfigException', false) || require 'rthink/config/ConfigException.php';
            throw new ConfigException ('Filename is not set');
        }

        $ini_array = $this->_parseIniFile($filename);

        if (!isset ($ini_array [$section])) {
            /**
             * @see ConfigException
             */
            class_exists('ConfigException', false) || require 'rthink/config/ConfigException.php';
            throw new ConfigException ("Section '$section' cannot be found in $filename");
        }

        $section_data = $this->_processSection($ini_array, $section);

        parent::__construct($section_data, $allow_modify);

        $this->_loaded_section = $section;
    }


    /**
     * get
     * 获取指定key的配置项 不存在则返回空
     *
     * @access public
     * @param string $name
     * @param mixed $default
     *
     * @return mixed
     */
    public function get($key, $default = null)
    {

        $result = $default;

        if (array_key_exists($key, $this->_data)) {
            $result = $this->_data [$key];
        } else {

            list($key1, $key2) = explode('.', $key);

            if (isset($this->_data[$key1][$key2])) {
                $result = $this->_data[$key1][$key2];
            }
        }

        return $result;
    }


    /**
     * _parseIniFile Load the INI file from disk using parse_ini_file(). Use a
     * private error handler to convert any loading errors into a
     * ConfigException
     *
     * @access protected
     * @param string $filename
     * @throws ConfigException
     * @return array
     */
    protected function _parseIniFile($filename)
    {
        set_error_handler(array(
            $this,
            '_loadFileErrorHandler'
        ));
        $ini_array = parse_ini_file($filename, true);
        restore_error_handler();

        // Check if there was a error while loading file
        if ($this->_error_str !== null) {
            class_exists('ConfigException', false) || require 'rthink/config/ConfigException.php';
            throw new ConfigException ($this->_error_str);
        }

        return $ini_array;
    }

    /**
     * _processSection 获取指定配置片段
     *
     * @access protected
     * @param array $ini_array
     * @param string $section
     *
     * @return array
     */
    protected function _processSection($ini_array, $section)
    {
        $process_data = array();

        foreach ($ini_array[$section] as $key => $val) {
            $group_key = explode('.', $key);

            if (count($group_key) > 3) {
                class_exists('ConfigException', false) || require 'rthink/config/ConfigException.php';
                throw new ConfigException ('配置文件数据节点层次支持深度为最大为3层！');
            }

            if (isset($group_key[2])) {
                $process_data[$group_key[0]][$group_key[1]][$group_key[2]] = $val;
            } else if (isset($group_key[1])) {
                $process_data[$group_key[0]][$group_key[1]] = $val;
            }
        }


        return array_merge($ini_array[$section], $process_data);
    }
}
