<?php

abstract class RThinkLoader
{
    abstract public static function factory($class_name, array $option);
}