<?php
require 'rthink/Loader/RThinkLoader.php';

class ModelLoader extends RThinkLoader
{

    public static function factory($model, array $option = array())
    {

        $model_class = ucfirst($model) . 'Model';

        class_exists('Model', false) ||
        require PATH_LIB . '/rthink/model/Model.php';
        class_exists($model, false) ||
        require PATH_APP . '/models/' . $model_class . '.php';

        return $model_class::getInstance();
    }

}