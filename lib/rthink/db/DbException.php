<?php
/** @see rthinkException * */
require_once 'rthink/exception/rthinkException.php';

/**
 * 数据库异常类
 */
class DbException extends RThinkException
{
}