<?php
/** @see rthinkException * */
require_once 'rthink/exception/RThinkException.php';

/**
 * 控制器异常
 */
class ControllerException extends RThinkException
{
}