<?php
/** ControllerException */
require_once 'rthink/controller/ControllerException.php';

/**
 * 分发器异常
 */
class ControllerDispatcherException extends ControllerException
{
}

