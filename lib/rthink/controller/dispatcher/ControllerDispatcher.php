<?php

/**
 * 分发器
 */
class ControllerDispatcher
{

    /**
     * 默认action
     *
     * @var string
     */
    protected $_default_action = 'index';

    /**
     * 默认controller
     *
     * @var string
     */
    protected $_default_controller = 'index';

    /**
     * 默认module
     *
     * @var string
     */
    protected $_default_module = 'default';

    /**
     * 前端控制器实例
     *
     * @var ControllerFront
     */
    protected $_front_controller;

    /**
     * 实例化动作控制器时的调用参数
     *
     * @var array
     */
    protected $_invoke_params = array();

    /**
     * 传入动作控制器的请求对象
     *
     * @var Sys_Controller_Response
     */
    protected $_response = null;

    /**
     * 单词分割符
     *
     * @var array
     */
    protected $_word_delimiter = '-';

    /**
     * 当前分发的目录
     *
     * @var string
     */
    protected $_current_dir;

    /**
     * 当前模块(formatted)
     *
     * @var string
     */
    protected $_current_module;

    /**
     * 动作控制器目录
     *
     * @var array
     */
    protected $_controller_dir = array();

    /**
     * 构造方法 将默认模块设置为当前模块
     *
     * @param array $params
     * @return void
     */
    public function __construct(array $params = array())
    {
        $this->setParams($params);
        $this->_current_module = $this->_default_module;
    }


    /**
     * 获取当前动作控制器的路径 如果指定了模块则返回指定模块的下的动控制器路径
     *
     * @param string $module Module name
     * @return array string array of all directories by default, single module
     *         directory if module argument provided
     */
    public function getControllerDirectory($module = null)
    {
        if (null === $module) {
            return $this->_controller_dir;
        }

        if (array_key_exists($module, $this->_controller_dir)) {
            return $this->_controller_dir [$module];
        }

        return null;
    }

    /**
     * 设置控制器路径
     *
     * @param array $directory 控制器路径
     *  <code>
     *      array( array('path' => 控制路径,
     *                            'module' => 所属模块),
     *              array('path' => '', 'module' => ''),
     *      )
     *  </code>
     * @return ControllerDispatcher
     */
    public function setControllerDirectory(array $dirs = array())
    {
        $this->_controller_dir = array();

        foreach ($dirs as $dir) {
            if (null == $dir ['module']) {
                $dir ['module'] = $this->_default_module;
            }

            $this->_controller_dir [strval($dir ['module'])] = rtrim(strval($dir ['path']), '/\\');
        }

        return $this;
    }

    public function getWordDelimiter()
    {
        return $this->_word_delimiter;
    }

    /**
     * 格式化模块名
     *
     * @param string $unformatted
     * @return string
     */
    public function formatModuleName($unformatted)
    {
        if ($this->_default_module == $unformatted) {
            return $unformatted;
        }

        return $this->_formatName($unformatted);
    }

    /**
     * 格式化动作类名
     *
     * @param string $module_name 当前模块名
     * @param string $class_name 动作类名
     * @return string Formatted class name
     */
    public function formatClassName($module_name, $class_name)
    {
        return $this->formatModuleName($module_name) . $class_name;
    }

    /**
     * 如果request对象可以被分到一个控制器则返回true
     * Use this method wisely. By default, the dispatcher will fall back to the
     * default controller (either in the module specified or the global default)
     * if a given controller does not exist. This method returning false does
     * not necessarily indicate the dispatcher will not still dispatch the call.
     *
     * @param ControllerRequest $action
     * @return false | string
     */
    public function isDispatchable(ControllerRequest $request)
    {
        $class_name = $this->getControllerClass($request);

        if ($this->_default_module != $this->_current_module) {
            $class_name = $this->formatClassName($this->_current_module, $class_name);
        }

        if (class_exists($class_name, false)) {
            return $class_name;
        }

        $load_file = $this->getDispatchDirectory() . DIRECTORY_SEPARATOR . $class_name . '.php';

        if (!is_readable($load_file)) {
            return false;
        }

        class_exists($class_name, false) || require $load_file;

        if (!class_exists($class_name, false)) {
            return false;
        }

        return $class_name;
    }

    /**
     * 分发到一个 controller/action 指定的动作分发器不能分发则抛出异常
     *
     * @param ControllerRequest $request
     * @param ControllerResponse $response
     * @return void
     * @throws ControllerDispatcherException
     */
    public function dispatch(ControllerRequest $request, ControllerResponse $response)
    {
        $this->setResponse($response);

        /**
         * 获取动作控制器类
         */
        if (false == ($class_name = $this->isDispatchable($request))) {
            class_exists('ControllerDispatcherException.php', false) || require 'rthink/controller/dispatcher/ControllerDispatcherException.php';
            throw new ControllerDispatcherException ('Invalid controller class : ' . $this->getControllerClass($request));
        }

        /**
         * 实例化动作控制器 如果指定动作控制器不是ControllerAction的实例则抛出异常
         */
        $controller = new $class_name ($request, $response, $this->getParams());

        if (!($controller instanceof ControllerAction)) {
            class_exists('ControllerDispatcherException', false) || require 'rthink/controller/dispatcher/ControllerDispatcherException.php';
            throw new ControllerDispatcherException ('Controller "' . $class_name . '" is not an instance of ControllerAction');
        }

        /**
         * 获取action名称
         */
        $action = $this->getActionMethod($request);

        $controller->setInvokeArg('view_name', substr($action, 0, -6));
        /**
         * Dispatch the method call
         */
        $request->setDispatched(true);

        // by default, buffer output
        $disable_ob = $this->getParam('disableOutputBuffering');

        $obLevel = ob_get_level();

        if (null === $disable_ob) {
            ob_start();
        }

        try {
            $controller->dispatch($action);
        } catch (Exception $e) {
            // Clean output buffer on error
            $curObLevel = ob_get_level();

            if ($curObLevel > $obLevel) {
                do {
                    ob_get_clean();
                    $curObLevel = ob_get_level();
                } while ($curObLevel > $obLevel);
            }
            throw $e;
        }

        if (empty ($disable_ob)) {
            $content = ob_get_clean();
            $response->appendBody($content);
        }

        // 销毁当前页的动作控制器实例和反射对象
        $controller = null;
    }

    /**
     * 获取控制器类名
     * Try request first; if not found, try pulling from request parameter; if
     * still not found, fallback to default
     *
     * @param ControllerRequest $request
     * @return string false class name on success
     */
    public function getControllerClass(ControllerRequest $request)
    {
        $controller_name = $request->getControllerName();
        $class_name = $this->formatControllerName($controller_name);
        $controller_dirs = $this->getControllerDirectory();
        $module = $request->getModuleName();


        if (false == $this->isValidModule($module)) {
            class_exists('ControllerDispatcherException', false) || require 'rthink/controller/dispatcher/ControllerDispatcherException.php';
            throw new ControllerDispatcherException ('No default module defined for this application');
        }

        $this->_current_module = $module;
        $this->_current_dir = $controller_dirs [$module];

        return $class_name;
    }

    /**
     * 检测给定的模块是否有效
     *
     * @param string $module
     * @return bool
     */
    public function isValidModule($module)
    {
        if (!is_string($module)) {
            return false;
        }

        $module = strtolower($module);
        $controller_dir = $this->getControllerDirectory();

        foreach (array_keys($controller_dir) as $module_name) {
            if ($module == strtolower($module_name)) {
                return true;
            }
        }

        return false;
    }

    /**
     * 获取当前派遣路径即模块 (as set by {@link getController()})
     *
     * @return string
     */
    public function getDispatchDirectory()
    {
        return $this->_current_dir;
    }

    /**
     * 设定请求action名称
     * First attempt to retrieve from request; then from request params using
     * action key; default to default action
     * Returns formatted action name
     *
     * @param ControllerRequest $request
     * @return string
     */
    public function getActionMethod(ControllerRequest $request)
    {
        $action = $request->getActionName();
        if (empty ($action)) {
            $action = $this->_default_action;
            $request->setActionName($action);
        }

        return $this->formatActionName($action);
    }

    /**
     * 格式化动作控制名称
     *
     * @param string $unformatted
     * @return string
     */
    public function formatControllerName($unformatted)
    {
        return $this->_formatName($unformatted) . 'Controller';
    }

    /**
     * 格式化action名称
     * This is used to take a raw action name, such as one that would be stored
     * inside a Sys_Controller_Request object, and reformat into a proper method
     * name that would be found inside a class extending Sys_Controller_Action.
     *
     * @param string $unformatted
     * @return string
     */
    public function formatActionName($unformatted)
    {
        $formatted = $this->_formatName($unformatted);
        return strtolower(substr($formatted, 0, 1)) . substr($formatted, 1) . 'Action';
    }


    /**
     * Formats a string from a URI into a PHP-friendly name.
     * By default, replaces words separated by the word separator character(s)
     * with camelCaps. If $is_action is false, it also preserves replaces words
     * separated by the path separation character with an undersrthink, making the
     * following word Title cased. All non-alphanumeric characters are removed.
     *
     * @param string $unformatted
     * @param boolean $is_action Defaults to false
     * @return string
     */
    protected function _formatName($unformatted)
    {
        $unformatted = str_replace($this->_word_delimiter, ' ', strtolower($unformatted));
        $unformatted = preg_replace('/[^a-z0-9 ]/', '', $unformatted);
        return str_replace(' ', '', ucwords($unformatted));
    }

    /**
     * 获取前端控制器实例
     *
     * @return ControllerFront
     */
    public function getFrontController()
    {
        if (null === $this->_front_controller) {
            class_exists('ControllerFront') || require 'rthink/controller/ControllerFront.php';
            $this->_front_controller = ControllerFront::getInstance();
        }

        return $this->_front_controller;
    }

    /**
     * 添加/修改动作控制器的参数
     *
     * @param string $key
     * @param mixed $val
     * @return Sys_Controller_Dispatcher
     */
    public function setParam($name, $value)
    {
        $this->_invoke_params [$name] = $value;
        return $this;
    }

    /**
     * 设置传递给动作控制器构造方法的参数
     *
     * @param array $params
     * @return ControllerDispatcher
     */
    public function setParams(array $params)
    {
        $this->_invoke_params = array_merge($this->_invoke_params, $params);

        return $this;
    }

    /**
     * 获取指定的动作控制器参数
     *
     * @param string $name
     * @return mixed
     */
    public function getParam($name)
    {
        if (isset ($this->_invoke_params [$name])) {
            return $this->_invoke_params [$name];
        }
        return null;
    }

    /**
     * 获取动作控制器实例化时的参数
     *
     * @return array
     */
    public function getParams()
    {
        return $this->_invoke_params;
    }


    /**
     * 设置传递给动作控制器的响应对象
     *
     * @param ControllerResponse|null $response
     * @return ControllerDispatcher
     */
    public function setResponse(ControllerResponse $response = null)
    {
        $this->_response = $response;
        return $this;
    }

    /**
     * 获取注册的响应对象
     *
     * @return ControllerResponse null
     */
    public function getResponse()
    {
        return $this->_response;
    }

    /**
     * 获取默认的controller (minus formatting)
     *
     * @return string
     */
    public function getDefaultController()
    {
        return $this->_default_controller;
    }

    public function  getDefaultAction()
    {
        return $this->_default_action;
    }

    /**
     * 获取默认模块
     *
     * @return string
     */
    public function getDefaultModule()
    {
        return $this->_default_module;
    }
}
