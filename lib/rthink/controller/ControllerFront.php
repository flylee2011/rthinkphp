<?php

/**
 * 前端控制器
 */
class ControllerFront
{

    /**
     * 派遣器 实例
     *
     * @var ControllerDispatcher
     */
    protected $_dispatcher = null;

    /**
     * 前端控制器单例实例
     *
     * @var ControllerFront
     */
    protected static $_instance = null;

    /**
     * 实例化动作控制器的请求参数
     *
     * @var array
     */
    protected $_invoke_params = array();

    /**
     * ControllerPluginBroker实例
     *
     * @var ControllerPluginBroker
     */
    protected $_plugins = null;

    /**
     * ControllerRequest 实例
     *
     * @var ControllerRequest
     */
    protected $_request = null;

    /**
     * ControllerResponse 实例
     *
     * @var ControllerResponse
     */
    protected $_response = null;

    /**
     * 派遣时是否在渲染输出之前返回响应对象 默认发送头信息和渲染输出 {@link dispatch()}
     *
     * @var boolean
     */
    protected $_return_response = false;

    /**
     * 路由器实例
     *
     * @var ControllerRouter
     */
    protected $_router = null;

    /**
     * 发生异常时直接抛出还是将异常信息收集在响应对象中 {@link dispatch()}
     *
     * @var boolean
     */
    protected $_throw_exceptions = false;

    /**
     * 构造方法 实例化 plugin broker.
     *
     * @return void
     */
    protected function __construct()
    {
        require 'rthink/controller/ControllerAction.php';
    }

    /**
     * 禁止克隆，强制使用单例
     *
     * @return void
     */
    private function __clone()
    {
    }

    /**
     * 获取单例
     *
     * @return ControllerFront
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self ();
        }

        spl_autoload_register(array(self::$_instance, 'loader'));

        return self::$_instance;
    }

    /**
     * 设置控制器目录
     *
     * @param array $dirs 动作控制器路径
     * <code>
     * array(
     *     array('path' => 控制路径,   'module'   => 所属模块),
     * array('path' => '', 'module' => ''),
     * )
     * </code>
     * @return ControllerFront
     */
    public function setControllerDirectory(array $dirs)
    {
        $this->getDispatcher()->setControllerDirectory($dirs);
        return $this;
    }

    /**
     * 获取动作控制器目录指定模块控制器目录 当$module_name为null时返回所有的目录
     *
     * @param string $module_name 模块名 默认 null
     * @return array string null
     */
    public function getControllerDirectory($module_name = null)
    {
        return $this->getDispatcher()->getControllerDirectory($module_name);
    }

    /**
     * 获取请求对象
     *
     * @return ControllerRequest
     */
    public function getRequest()
    {
        if (null === $this->_request) {
            class_exists('ControllerRequest', false) || require 'rthink/controller/request/ControllerRequest.php';
            $this->_request = new ControllerRequest();
        }
        return $this->_request;
    }

    /**
     * 获取响应对象
     *
     * @return ControllerResponse
     */
    public function getResponse()
    {
        if (null === $this->_response) {
            class_exists('ControllerResponse', false) || require 'rthink/controller/response/ControllerResponse.php';
            $this->_response = new ControllerResponse ();
        }
        return $this->_response;
    }

    /**
     * 获取路由器对象
     *
     * @return ControllerRouter
     */
    public function getRouter()
    {
        if (null === $this->_router) {
            class_exists('ControllerRouter', false) || require 'rthink/controller/router/ControllerRouter.php';
            $this->_router = new ControllerRouter ();
        }
        return $this->_router;
    }

    /**
     * 获取派遣器对象
     *
     * @return ControllerDispatcher
     */
    public function getDispatcher()
    {
        if (null === $this->_dispatcher) {
            class_exists('ControllerDispatcher', false) || require 'rthink/controller/dispatcher/ControllerDispatcher.php';
            $this->_dispatcher = new ControllerDispatcher();
        }
        return $this->_dispatcher;
    }

    /**
     * 添加/修改实例化动作控制器时的参数
     *
     * @param string $name
     * @param mixed $value
     * @return ControllerFront
     */
    public function setParam($name, $value)
    {
        $name = strval($name);
        $this->_invoke_params [$name] = $value;
        return $this;
    }

    /**
     * 设置传提给动作控制器构造方法的参数
     *
     * @param array $params
     * @return ControllerFront
     */
    public function setParams(array $params)
    {
        $this->_invoke_params = array_merge($this->_invoke_params, $params);
        return $this;
    }

    /**
     * 获取动作控制器参数
     *
     * @param string $name
     * @return mixed
     */
    public function getParam($name)
    {
        $name = strval($name);

        if (isset ($this->_invoke_params [$name])) {
            return $this->_invoke_params [$name];
        }

        return null;
    }

    /**
     * 获取实例化动作控制器时的参数
     *
     * @return array
     */
    public function getParams()
    {
        return $this->_invoke_params;
    }

    /**
     * 设置抛出异常标识 and 获取异常标识 设置在派遣中异常抛出还是分装在response对象中,默认分装在response对象
     *
     * @param boolean $flag
     * @return boolean ControllerFront
     */
    public function throwExceptions($flag = null)
    {
        if ($flag !== null) {
            $this->_throw_exceptions = ( bool )$flag;
            return $this;
        }

        return $this->_throw_exceptions;
    }

    /**
     * Set whether {@link dispatch()} should return the response without first
     * rendering output. By default, output is rendered and dispatch() returns
     * nothing.
     *
     * @param boolean $flag
     * @return boolean ControllerFront as a setter, returns object; as a getter,
     *         returns boolean
     */
    public function returnResponse($flag = null)
    {
        if (true === $flag) {
            $this->_return_response = true;
            return $this;
        } elseif (false === $flag) {
            $this->_return_response = false;
            return $this;
        }

        return $this->_return_response;
    }

    /**
     * 派遣请求到 controller/action.
     *  处理线索 dispatch(ControllerFront) ->dispatch(ControllerDispatcherStandard) -> dispatch(ControllerAction)
     * 最终还是交由动作控制器自己分发
     *
     * @return void ControllerResponse response object if returnResponse() is true
     */
    public function dispatch()
    {
        /**
         * 获取请求对象
         */
        if (null === $this->_request) {
            $this->getRequest();
        }

        /**
         * 获取响应对象
         */
        if (null === $this->_response) {
            $this->getResponse();
        }

        /**
         * 初始化路由器
         */
        $router = $this->getRouter();


        /**
         * 初始化派遣器
         */
        $dispatcher = $this->getDispatcher();
        $dispatcher->setParams($this->getParams())->setResponse($this->_response);

        // 开始派遣
        try {
            try {
                $router->route($this->_request);
            } catch (Exception $e) {
                if ($this->throwExceptions()) {
                    throw $e;
                }
                $this->_response->setException($e);
            }


            /**
             * 尝试派遣controller/action.如果 $this->_request表明需要被派遣，
             * 移动到request的下一个action
             */
            do {
                $this->_request->setDispatched(true);

                /**
                 * 如果 preDispatch() 重置了请求的action则跳过
                 */
                if (false == $this->_request->isDispatched()) {
                    continue;
                }

                /**
                 * 派遣请求
                 */
                try {
                    $dispatcher->dispatch($this->_request, $this->_response);
                } catch (Exception $e) {
                    if ($this->throwExceptions()) {
                        throw $e;
                    }
                    $this->_response->setException($e);
                }

            } while (false == $this->_request->isDispatched());

        } catch (Exception $e) {
            $e->request_params = $this->_request->getParams();

            if ($this->throwExceptions()) {
                throw $e;
            }

            $this->_response->setException($e);
        }

        if ($this->_request->getParam('rdebug') !== null) {
            Debug::requestDebug();
        }

        if ($this->returnResponse()) {
            return $this->_response;
        }

        $this->_response->sendResponse();
    }

    protected function loader($class_name)
    {
        $loaders = $this->getParam('loader');

        if (in_array($class_name, $loaders)) {
            require 'rthink/Loader' . '/' . $class_name . '.php';
            return new $class_name();
        } else if ($class_name == 'Config') {
            require 'rthink/config/Config.php';
        } else if ($class_name == 'Debug') {
            require 'rthink/debug/Debug.php';
        }

    }
}