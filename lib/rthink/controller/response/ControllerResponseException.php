<?php
/** @see ControllerException */
require_once 'rthink/controller/ControllerException.php';

/**
 * 响应对象异常
 */
class ControllerResponseException extends ControllerException
{
}

