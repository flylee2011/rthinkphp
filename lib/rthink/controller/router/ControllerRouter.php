<?php

/**
 * 路由
 */
class ControllerRouter
{
    /**
     * URI分隔符
     */
    const URI_DELIMITER = '/';

    /**
     * 前端控制器实例
     *
     * @var ControllerFront
     */
    protected $_front_controller;

    /**
     * 获取前端控制器
     *
     * @return ControllerFront
     */
    public function getFrontController()
    {
        // Used cache version if found
        if (null !== $this->_front_controller) {
            return $this->_front_controller;
        }

        class_exists('ControllerFront') || require 'rthink/controller/ControllerFront.php';
        $this->_front_controller = ControllerFront::getInstance();
        return $this->_front_controller;
    }


    /**
     * 从用户提交的PATH_INFO匹配 模块 控制器 action 和 其它参数信息并返回一个关联数组 If a request object is
     * registered, it uses its setModuleName(), setControllerName(), and
     * setActionName() accessors to set those values. Always returns the values
     * as an array.
     *
     * @param string $path Path used to match against this routing map
     * @return array An array of assigned values or a false on a mismatch
     */
    protected function _match($path)
    {

        $values = array();
        $params = array();

        $path = trim($path, self::URI_DELIMITER);

        $path = explode(self::URI_DELIMITER, $path);

        if ($this->getFrontController()->getDispatcher()->isValidModule($path [0])) {
            $values ['module'] = array_shift($path);
        }

        if (count($path) && !empty ($path [0])) {
            $values ['controller'] = array_shift($path);
        }

        if (count($path) && !empty ($path [0])) {
            $values ['action'] = array_shift($path);
        }

        if ($num_segs = count($path)) {
            for ($i = 0; $i < $num_segs; $i = $i + 2) {
                $params [urldecode($path [$i])] = isset ($path [$i + 1]) ? urldecode($path [$i + 1]) : null;
            }
        }

        $default = array(
            'module' => $this->getFrontController()->getDispatcher()->getDefaultModule(),
            'controller' => $this->getFrontController()->getDispatcher()->getDefaultController(),
            'action' => $this->getFrontController()->getDispatcher()->getDefaultAction()
        );

        return $values + $params + $default;
    }

    protected function _setRequestParams($request, $params)
    {
        foreach ($params as $param => $value) {

            $request->setParam($param, $value);

            if ($param === $request->getModuleKey()) {
                $request->setModuleName($value);
            }
            if ($param === $request->getControllerKey()) {
                $request->setControllerName($value);
            }
            if ($param === $request->getActionKey()) {
                $request->setActionName($value);
            }
        }
    }

    /**
     * 从当前PATH_INFO查找匹配路由 并将返回值保存在请求对象中
     *
     * @throws ControllerRouterException
     * @return ControllerRequest Request object
     */
    public function route(ControllerRequest $request)
    {
        $match = $request->getPathInfo();

        if ($params = $this->_match($match)) {
            $this->_setRequestParams($request, $params);
        } else {
            class_exists('ControllerRouterException', false) || require 'rthink/controller/router/ControllerRouterException.php';
            throw new ControllerRouterException ('No route matched the request', 404);
        }

        return $request;
    }


}
