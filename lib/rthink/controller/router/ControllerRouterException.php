<?php
/** ControllerException */
require_once 'rthink/controller/ControllerException.php';

/**
 * 路由器异常
 */
class ControllerRouterException extends ControllerException
{
}