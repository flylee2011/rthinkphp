<?php

/** @see ControllerException */
require_once 'rthink/controller/ControllerException.php';

/**
 * 动作助手异常类
 */
class ControllerActionException extends ControllerException
{
}
