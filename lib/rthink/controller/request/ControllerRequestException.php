<?php
/** ExceptionController */
require_once 'rthink/controller/ControllerException.php';

/**
 * 请求控制器异常
 */
class ControllerRequestException extends ControllerException
{
}

