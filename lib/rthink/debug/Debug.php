<?php

/**
 * Class Debug
 */
class Debug
{
    protected static $_front_controller = null;

    /**
     * rthinkphp debug
     *
     * @param string $info debug信息
     */
    public static function rtDebug($info = '')
    {
        if (!Config::get('debug')) {
            return;
        }

        $backtrace = array_shift(debug_backtrace());

        self::getFrontController()->getResponse()->prepend(md5(microtime()), '-->' . PHP_EOL);
        self::getFrontController()->getResponse()->prepend(md5(microtime()), 'Debug文件: ' . $backtrace['file'] . PHP_EOL);
        self::getFrontController()->getResponse()->prepend(md5(microtime()), '文件行号: ' . $backtrace['line'] . PHP_EOL);
        self::getFrontController()->getResponse()->prepend(md5(microtime()), 'Debug输出: ' . var_export($info, true) . PHP_EOL);
        self::getFrontController()->getResponse()->prepend(md5(microtime()), '<!--' . PHP_EOL);
    }

    /**
     * firephp debug
     *
     * @param string $info debug信息
     */
    public static function fbDebug($info = '')
    {
        if (!Config::get('debug')) {
            return;
        }

        require_once 'FirePHPCore/fb.php';

        fb($info, FirePHP::TRACE);
    }

    /**
     * 请求debug查看sql信息和请求参数
     */
    public static function requestDebug()
    {
        if (!Config::get('debug')) {
            return;
        }

        $debug_info = array();

        $db_profilers = self::getFrontController()->getRequest()->getParam('db_profilers');

        $total_query = 0;
        $total_time = 0.0;

        $query_detail = array();

        foreach ($db_profilers as $db_profiler) {
            $query_profiles = $db_profiler->getQueryProfiles();

            foreach ($query_profiles as $query_profile) {
                $query_detail[$query_profile->getQueryType()][] = array(
                    'sql' => $query_profile->getQuery(),
                    'time' => $query_profile->getElapsedSecs(),
                );
            }

            $total_query += $db_profiler->getTotalNumQueries();
            $total_time += $db_profiler->getTotalElapsedSecs();
        }

        if (!empty($db_profilers)) {
            foreach ($query_detail as $detail_item) {
                foreach ($detail_item as $item_val) {
                    $debug_info[] = sprintf('执行时间: %f; SQL: %s' . PHP_EOL, $item_val['time'], $item_val['sql']);
                }
            }

            $debug_info[] = sprintf('SQL语句总数: %d 总花费时间: %f 秒' . PHP_EOL, $total_query, $total_time);

            self::getFrontController()->getRequest()->clearParam('db_profilers');
        }

        $debug_info[] = sprintf('请求参数: ' . PHP_EOL . '%s' . PHP_EOL, var_export(self::getFrontController()->getRequest()->getParams(), true));

        self::getFrontController()->getResponse()->prepend(md5(microtime()), PHP_EOL . '-->');

        foreach ($debug_info as $debug_item) {
            self::getFrontController()->getResponse()->prepend(md5(microtime()), $debug_item);
        }

        self::getFrontController()->getResponse()->prepend(md5(microtime()), '<!--' . PHP_EOL);
    }

    /**
     * 获取前端控制器
     *
     * @return ControllerFront
     */
    protected static function getFrontController()
    {
        // Used cache version if found
        if (null === self::$_front_controller) {
            class_exists('ControllerFront') || require 'rthink/ControllerFront.php';
            self::$_front_controller = ControllerFront::getInstance();
        }

        return self::$_front_controller;
    }
}