<?php
/**
 * Class TestModel 数据test的操作类
 */
class TestModel extends Model {

    protected static $_instance = null;

    public function __construct() {
        $db_conf = Config::get('db.test');

        parent::__construct($db_conf);
    }

    public static function getInstance() {
        if (null == self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

}