<?php

/**
 * index 控制器
 *
 * Class IndexController
 */
class IndexController extends ControllerAction
{

    public function indexAction()
    {
        echo 'Hello World!';
    }

    public function viewAction()
    {
        $data = array();

        $data['title'] = '视图渲染';
        $key = $this->getRequest()->getParam('key');
        $data['key'] = $key;

        $this->render($data);
    }

    public function viewLayoutAction()
    {
        $data = array();

        $data['title'] = '视图渲染';
        $key = $this->getRequest()->getParam('key');
        $data['key'] = $key;
        $this->setInvokeArg('layout', 'mainLayout');

        $this->render($data);
    }


    public function addAction()
    {
        $data = array();

        $data['title'] = '数据添加';
        $this->setInvokeArg('layout', 'mainLayout');

        $this->render($data);
    }


    public function addDoAction()
    {
        $title = $this->getRequest()->getPost('title');
        $author = $this->getRequest()->getPost('author');
        $content = $this->getRequest()->getPost('content');

        $test_model = ModelLoader::factory('test');
        $test_model->setHandleTable('story');

        $data = array(
            'title' => $title,
            'author' => $author,
            'content' => $content,
            'create_time' => date('Y-m-d H:i:s')
        );

        $test_model->add($data);

        $this->_forward('add');
    }


    public function updateAction()
    {
        $test_model = ModelLoader::factory('test');
        $test_model->setHandleTable('story');

        $data = array(
            'title' => 'cc',
            'content' => 'cc'
        );

        $condition = array(
            'id>' => 17,
            'create_time>' => '2014-05-12 20:14:21'
        );

        $upd_res = $test_model->update($data, $condition);

//        var_dump($upd_res);
    }


    public function deleteAction()
    {
        $test_model = ModelLoader::factory('test');
        $test_model->setHandleTable('story');

        $condition = array(
            'id>' => 18,
        );

        $upd_res = $test_model->delete($condition);

        var_dump($upd_res);
    }


    /**
     * 取回结果集中所有字段的值,作为连续数组返回
     */
    public function listAction()
    {
        $data = array();

        $data['title'] = '列表页面';

        $test_model = ModelLoader::factory('test');

        $test_model->setHandleTable('story');

        $condition = array(
            'where' => array(
                'id' => 1
            ),
        );
//

//        $condition = array(
//            'fields' => array('id', 'title', 'author', 'create_time'),
//            'where' => array(
////                'id >= ? and id <= ? or id >= ? and id<= ?' => array(1,2,3,4),
//                   'id in (?) or title = ?' => array(array(1,2,'St John"s Wort'), 'St John"s Wort')
//            ),
//            'limit' => array('count' => 2, 'offset' => 0)
//        );


        $data['story_list'] = $test_model->fetchAll($condition);
        $this->setInvokeArg('layout', 'mainLayout');

        $this->render($data, 'list');
    }

    /**
     * 只取回结果集的第一行
     */
    public function listRowAction()
    {
        $data = array();

        $data['title'] = '列表页面';

        $test_model = ModelLoader::factory('test');
        $test_model->setHandleTable('story');

        $condition = array(
            'fields' => array('title', 'author', 'create_time'),
        );

        $data['test'] = $test_model->fetchRow();
        $this->setInvokeArg('layout', 'mainLayout');

        Debug::rtDebug($data);
//        $this->render($data);
    }



    /**
     * 取回结果集中所有字段的值,作为关联数组返回
     * 第一个字段作为key
     */
    public function listAssocAction()
    {
        $data = array();

        $data['title'] = '列表页面';

        $test_model = ModelLoader::factory('test');
        $test_model->setHandleTable('story');

        $condition = array(
            'fields' => array('title', 'author', 'create_time'),
        );

        $data['story_list'] = $test_model->fetchAssoc($condition);
        $this->setInvokeArg('layout', 'mainLayout');


        var_dump($data);
//        $this->render($data, 'list');
    }


    /**
     * 取回所有结果行的第一个字段名
     */
    public function listColAction()
    {
        $data = array();

        $data['title'] = '列表页面';

        $test_model = ModelLoader::factory('test');
        $test_model->setHandleTable('story');

        $condition = array(
            'fields' => array('id'),
        );

        $data['story_list'] = $test_model->fetchCol($condition);
        $this->setInvokeArg('layout', 'mainLayout');

        var_dump($data);
    }


    public function configAction()
    {
        var_dump(Config::get('db'));
        var_dump(Config::get('db.test'));
        var_dump(Config::get('db.test.host'));
    }

    public function testAction()
    {
//        $this->_redirect('http://www.baidu.com');
        echo 'test';
    }
}