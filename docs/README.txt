README
======

你的vhost
=====================

<VirtualHost *:80>
   DocumentRoot "/var/www/htdocs/RThink/public"
   ServerName framework.local

   # This should be omitted in the production environment
   SetEnv APPLICATION_ENV development

   <Directory "/var/www/htdocs/RThink/public">
       Options Indexes MultiViews FollowSymLinks
       AllowOverride All
       Order allow,deny
       Allow from all
   </Directory>

</VirtualHost>
