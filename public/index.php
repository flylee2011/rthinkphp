<?php
/**
 * app 入口文件
 */
defined('PATH_ROOT') || define ('PATH_ROOT', realpath(dirname(__FILE__) . '/../'));

require PATH_ROOT . '/inc/conf/config_global.php';
require PATH_ROOT . '/inc/app.function.php';
ini_set('include_path', PATH_LIB);
require 'rthink/controller/ControllerFront.php';


$controller_front = ControllerFront::getInstance();

$controller_front->throwExceptions(true);

$controller_dirs = array();
array_push($controller_dirs, array(
    'path' => PATH_APP . '/controllers',
    'module' => 'default'
));
array_push($controller_dirs, array(
    'path' => PATH_APP . '/controllers/user',
    'module' => 'user'
));

$controller_front->setControllerDirectory($controller_dirs);

$controller_front->setParams(array(
        'config_file' => PATH_ROOT . '/inc/conf/app.ini',
        'config_section' => 'development',
        'view_path' => PATH_APP . '/views',
        'layout_path' => PATH_APP . '/layouts',
        'loader' => array('ModelLoader')
    )
);

//$controller_front->setParam ( 'config_file', PATH_ROOT . '/inc/conf/app.ini' );
//$controller_front->setParam ( 'config_section', 'production' //$controller_front->setParam('loader', array('ModelLoader'));

$controller_front->dispatch();